package fr.isitc.vandv


fun beginAnalyse(path: String) {

    // On commence par récuperer toutes les informations dont on a besoin sur le projet
    val projectClasses = Extractor().extractInfos(path)
    println("Il y a ${projectClasses.productionClasses.size} classes de production")
    println("Il y a ${projectClasses.testClasses.size} classes de test")

    // On execute les tests une première fois avant modification
    val testExecutor = TestExecutor(projectClasses)
    val results = testExecutor.doTesting()
    println("Il y a ${results.map { it.failures.size }.sum()} test(s) en erreur(s) pour ${results.map { it.runCount }.sum()} tests.")

    // On effectue toutes les modifications possibles
    val modifications = Mutator().computeMutation(projectClasses, testExecutor)

    // On restitue les résultats des différentes mutation
    Restitution().showResults(modifications)
}
