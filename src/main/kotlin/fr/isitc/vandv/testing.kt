package fr.isitc.vandv

import javassist.CtClass
import org.junit.runner.JUnitCore
import org.junit.runner.Result
import java.io.File
import java.net.URLClassLoader


/**
 * S'occupe d'executer les tests du projet cible
 */
class TestExecutor(private val projectClasses: ProjectClasses) {

    fun doTesting(): List<Result> {

        val classUrlTest = File(projectClasses.testRootPath).toURI().toURL()
        val classUrlProd = File(projectClasses.productionRootPath).toURI().toURL()
        val classUrlProject = arrayOf(classUrlTest, classUrlProd)

        val customClassLoader = URLClassLoader(classUrlProject, javaClass.classLoader)

        return commonExecutor(customClassLoader)
    }

    fun doTestingWithCustomClass(ctClass: CtClass, className: String): List<Result> {
        val tmpDir = createTempDir("ztmp_target")
        val tmpDirAbsolutePath = tmpDir.absolutePath

        val newTestDir = File("$tmpDirAbsolutePath/test-classes")
        val newProductionDir = File("$tmpDirAbsolutePath/classes")

        File(projectClasses.testRootPath).copyRecursively(newTestDir)
        File(projectClasses.productionRootPath).copyRecursively(newProductionDir)

        val oldClassPath = newProductionDir.absolutePath + "/" + className.replace(".", "/") + ".class"
        val oldClass = File(oldClassPath)

        if (!oldClass.exists()) {
            throw RuntimeException("Impossible de trouver la classe à remplacer")
        }

        if (!oldClass.delete()) {
            throw RuntimeException("Impossible de supprimer l'ancienne classe")
        }

        ctClass.writeFile(newProductionDir.absolutePath)

        val classUrlTest = newTestDir.toURI().toURL()
        val classUrlProd = newProductionDir.toURI().toURL()
        val classUrlProject = arrayOf(classUrlTest, classUrlProd)
        val customClassLoader = URLClassLoader(classUrlProject, javaClass.classLoader)

        return commonExecutor(customClassLoader)
    }

    private fun commonExecutor(customClassLoader: URLClassLoader): List<Result> {

        return projectClasses.testClasses.map {

            // Charge la classe souhaité
            val clazz = customClassLoader.loadClass(it)

            // Execute le test via le runner Junit
            JUnitCore().run(clazz)
        }
    }
}
