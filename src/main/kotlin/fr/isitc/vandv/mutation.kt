package fr.isitc.vandv

import javassist.ClassPool
import javassist.CtClass
import javassist.CtMethod
import javassist.NotFoundException
import javassist.bytecode.Opcode


abstract class ModificationInfo(
    val method: CtMethod,
    val originalClass: String
)

class OpCodeModificationInfo(method: CtMethod, originalClass: String, val originalOpcode: Byte, val indexIndice: Int) : ModificationInfo(method, originalClass)

enum class TypeMod {
    VOID,
}

class MethodModificationInfo(method: CtMethod, originalClass: String, val typeMod: TypeMod) : ModificationInfo(method, originalClass)


class Mutator {

    /**
     * Définit les correspondances des mutations simple
     */
    private val pairs: List<Pair<Int, Int>> = listOf(
        Pair(Opcode.DADD, Opcode.DSUB),
        Pair(Opcode.IADD, Opcode.ISUB),
        Pair(Opcode.FADD, Opcode.FSUB),
        Pair(Opcode.LADD, Opcode.LSUB),
        Pair(Opcode.DSUB, Opcode.DADD),
        Pair(Opcode.ISUB, Opcode.IADD),
        Pair(Opcode.FSUB, Opcode.FADD),
        Pair(Opcode.LSUB, Opcode.LADD),
        Pair(Opcode.DMUL, Opcode.DDIV),
        Pair(Opcode.IMUL, Opcode.IDIV),
        Pair(Opcode.FMUL, Opcode.FDIV),
        Pair(Opcode.LMUL, Opcode.LDIV),
        Pair(Opcode.DDIV, Opcode.DMUL),
        Pair(Opcode.IDIV, Opcode.IMUL),
        Pair(Opcode.FDIV, Opcode.FMUL),
        Pair(Opcode.LDIV, Opcode.LMUL),
        Pair(Opcode.IF_ICMPGE, Opcode.IF_ICMPGT),
        Pair(Opcode.IF_ICMPLE, Opcode.IF_ICMPLT),
        Pair(Opcode.IF_ICMPGT, Opcode.IF_ICMPGE),
        Pair(Opcode.IF_ICMPLT, Opcode.IF_ICMPLE)
    )

    /**
     * Prépare les mutations, les exécutes puis retourne la une liste de RecapModif
     */
    fun computeMutation(projectClasses: ProjectClasses, testExecutor: TestExecutor): List<RecapModif> {
        val recapitulations = mutableListOf<RecapModif>()

        projectClasses.productionClasses
            .forEach { className ->
                val pool = ClassPool(true)
                pool.appendClassPath(projectClasses.productionRootPath)

                val ctClass = pool.get(className)

                ctClass.methods.filter {

                    // On ne fais pas de mutation pour les méthodes de la classe parent
                    try {
                        val superclass = ctClass.superclass
                        superclass == null || !superclass.methods.toList().contains(it)
                    } catch (e: NotFoundException) {
                        // Si il n'y a pas de class parent alors il n'y a pas de méthode héritée
                        true
                    }

                }.forEach { currentMethod ->


                    // Si jamais la classe bloque
                    if (ctClass.isFrozen) {
                        ctClass.defrost()
                    }


                    val code = currentMethod.methodInfo.codeAttribute.code

                    loop@ for (i in code.indices) {


                        val opCode = code[i]

                        // On cherche une mutation correspondante à l'opération bytecode
                        val pair = this.pairs.find { originalOpCode -> opCode == originalOpCode.first.toByte() }
                        // Si l'on n'a pas de mutation à effectuer on passe à l'itération suivante de la boucle for
                            ?: continue@loop

                        code[i] = pair.second.toByte()

                        // On sauvegarde la modification effectué
                        val modInfo = OpCodeModificationInfo(currentMethod, className, opCode, i)

                        // On exécute les tests avec la modification
                        val results2 = testExecutor.doTestingWithCustomClass(ctClass, className)

                        recapitulations.add(
                            RecapModif(results2, modInfo)
                        )

                        defrostIfNecessary(ctClass)
                        rollbackModification(modInfo)
                        defrostIfNecessary(ctClass)
                    }
                    defrostIfNecessary(ctClass)

                    // On vérifie si la méthode à un type de retour void pour lui effectuer une mutation spécifique
                    if (currentMethod.returnType == CtClass.voidType) {
                        this.emptyBody(currentMethod)

                        val results = testExecutor.doTestingWithCustomClass(ctClass, className)

                        recapitulations.add(
                            RecapModif(results, MethodModificationInfo(currentMethod, className, TypeMod.VOID))
                        )
                    }
                }
            }

        return recapitulations.toList()
    }

    /**
     * Remet à l'état d'origine la modification
     */
    private fun rollbackModification(modInfo: OpCodeModificationInfo) {
        modInfo.method.methodInfo.codeAttribute.code[modInfo.indexIndice] = modInfo.originalOpcode
    }

    private fun reverseBoolean(code: ByteArray, indexIndice: Int) {
        // TODO trouver une façon d'inverser la valeur d'un booléen
        throw Exception("Not implemented Yet")
    }

    /**
     * Vide la méthode de tout code et la remplace par une instruction sans effet
     */
    private fun emptyBody(method: CtMethod) {
        method.setBody("return;")
    }

    /**
     * Dégèle la classe pour permettre une nouvelle modification
     */
    private fun defrostIfNecessary(ctClass: CtClass) {
        // Si jamais la classe bloque
        if (ctClass.isFrozen) {
            ctClass.defrost()
        }
    }

}
