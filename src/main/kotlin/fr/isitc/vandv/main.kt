package fr.isitc.vandv


open class App {
    companion object {

        @JvmStatic
        fun main(args: Array<String>) {
            if (args.size != 1) {
                throw RuntimeException("Aucun path vers un projet maven cible n'est fourni")
            }
            beginAnalyse(args[0])
        }
    }
}

