package fr.isitc.vandv

import org.junit.runner.Result

data class RecapModif(val result: List<Result>, val modInfo: ModificationInfo)


class Restitution {
    fun showResults(recaps: List<RecapModif>) {
        println("Il y a ${recaps.size} mutation(s) effectuée(s)")

        // TODO afficher ici la récapitulation des résultats
        // TODO faire des stats ?
        // TODO afficher uniquement les mutations qui ne produise pas d'erreur dans les tests ?
        recaps.forEach { recap ->

            val className = recap.modInfo.originalClass
            val start = "Modification de la classe $className pour la méthode ${recap.modInfo.method.name}"

            val modInfo = recap.modInfo
            if (modInfo is MethodModificationInfo) {
                println("$start de type ${modInfo.typeMod}")
            } else if (modInfo is OpCodeModificationInfo) {
                println("$start d'index ${modInfo.indexIndice} avec l'opCode ${modInfo.originalOpcode}")
            }
            recap.result.forEach {
                printResult(it)
            }
        }
    }

    private fun printResult(it: Result) {
        println("| run time : ${it.runTime}ms")
        println(String.format("| IGNORED: %d", it.ignoreCount))
        println(String.format("| FAILURES: %d", it.failureCount))
        println(String.format("| RUN: %d", it.runCount))
        println()
    }
}


