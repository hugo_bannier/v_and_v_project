package fr.isitc.vandv

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.stream.Collectors


data class ProjectClasses(
    val productionClasses: List<String>,
    val testClasses: List<String>,
    val productionRootPath: String,
    val testRootPath: String
)

class Extractor {

    fun extractInfos(path: String): ProjectClasses {
        val basePath = "$path/target"
        val productionClassesRootPath = "$basePath/classes"
        val testClassesRootPath = "$basePath/test-classes"

        return ProjectClasses(
            loadClassInfos(productionClassesRootPath),
            loadClassInfos(testClassesRootPath),
            productionClassesRootPath,
            testClassesRootPath
        )
    }

    fun loadClassInfos(folder: String): List<String> {
        val listClassName = arrayListOf<String>()
        val list = findClassesPath(folder)

        var newPath: String
        for (item in list) {
            newPath = item.toString().replace(folder, "").replace("/", ".")
                .replace(".class", "")
            if (newPath.substring(0, 1) == ".") {
                newPath = newPath.replaceFirst(".", "")
            }
            listClassName.add(newPath)
        }

        return listClassName

    }


    fun findClassesPath(path: String): List<Path> {

        if (!File(path).exists()) {
            throw RuntimeException("Le path passé en paramètre n'existe pas")
        }

        return Files.walk(Paths.get(path))
            .filter { s -> s.toString().endsWith(".class") }
            .sorted()
            .collect(Collectors.toList())
    }
}
