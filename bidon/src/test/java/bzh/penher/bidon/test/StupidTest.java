package bzh.penher.bidon.test;

import bzh.penher.bidon.StupidClass;
import bzh.penher.bidon.dto.DummyPojo;
import org.junit.Assert;
import org.junit.Test;

public class StupidTest {
    @Test
    public void shouldAlwaysPass() {
        Assert.assertTrue(new StupidClass().returnTrue());
    }

    @Test
    public void shouldUpdatePojo() {
        final int id = 42;
        final String name = "hugo";

        final DummyPojo dto = new DummyPojo(id, name);

        new StupidClass().resetPojo(dto);

        Assert.assertNotEquals("Les ids ne doivent pas être égaux", dto.getId(), id);
        Assert.assertNotEquals("Les noms ne doivent pas être égaux", name, dto.getName());
    }

    @Test
    public void shouldMultiplyNumbers() {
        final int result = new StupidClass().multiply(2, 2);

        Assert.assertEquals("Le résultat doit être une multiplication", 4, result);
    }

    @Test
    public void shouldAddNumbers() {
        final int result = new StupidClass().add(1, 1);

        Assert.assertEquals("Le résultat doit être une addition", 2, result);
    }

    @Test
    public void shouldSubstractNumbers() {
        final int result = new StupidClass().substract(4, 1);

        Assert.assertEquals("Le résultat doit être une soustraction", 3, result);
    }

    @Test
    public void shouldDividNumbers() {
        final double result = new StupidClass().divide(12d, 4d);

        Assert.assertEquals("Le résultat doit être une division", 0, Double.compare(3, result));
    }
}
