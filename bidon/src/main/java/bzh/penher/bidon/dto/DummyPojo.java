package bzh.penher.bidon.dto;

public class DummyPojo {
    private long id;
    private String name;

    public DummyPojo() {
    }

    public DummyPojo(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
