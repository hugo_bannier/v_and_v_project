# Projet V and V

## Binômes
* Hugo BANNIER
* Maxime DONGÉ

## Technologies
* [Kotlin](https://kotlinlang.org/)
* [Javassist](https://www.javassist.org/) 
* [Junit 4](https://junit.org/junit4/)
* [Maven 3](https://maven.apache.org/)

## Sujet  
[Mutation analysis](https://github.com/Software-Testing/Projects-2018-2019#mutation-analysis) : modifier les classes d'un projet pour vérifier la qualité des tests.
[Rapport](./Instructions_for_report.pdf) : Instruction pour rédiger le rapport qui accompagne le projet. 
    
Nous utilisons le projet [Apache Commons-collections](https://github.com/apache/commons-collections) comme cible en example.

## Utiliser le projet avec Maven
### Pré-requis
* JDK 8
* Maven 3

### Compiler le projet
```bash
    mvn compile
```

### Exécuter les tests
```bash
    mvn test
```

### Générer le jar
```bash
    mvn package
```

### Exécuter le jar
```bash
    java -jar target/v_and_v_project/target/v_and_v_project-1.0-SNAPSHOT.jar ./path/to/target_projet
```

## Script d'utilisation automatique en bash

### Pré-requis
* Linux
* JDK 8
* Maven 3
* Git

### Script
```bash
#!/bin/bash
mkdir tmp_build
cd tmp_build
# prépare le projet cible
git clone https://github.com/apache/commons-collections.git
cd commons-collections
mvn clean test
# récupération du projet
git clone https://gitlab.com/hugo_bannier/v_and_v_project.git
cd  v_and_v_project/
mvn clean install
# exécution du programme
java -jar target/v_and_v_project-1.0-SNAPSHOT-jar-with-dependencies.jar ../../commons-collections
```


## Référents

* oscar.vera-perez (arobase) inria (point) fr
* caroline.landry (arobase) inria (point) fr

## TODO
* Analyser l'argument de l'utilisateur (dans le main) et récuperer l'ensemble des classes compilés (tests et non-tests) -> Maxime
* Parcourir l'ensemble des classes non-test puis l'ensemble des méthodes puis l'ensemble des "lignes" du bytecode puis analyser si il y a des modifications à effectuer -> hugo
* sauvegarder la ligne avant de la modifier, effectuer la mutation possible d'une ligne, recharger la classe dans le classpath -> hugo
* executer l'ensemble des tests unitaires et garder les résultats des tests unitaires (resultat du runner + info sur la mutation)
* restaurer son état d'origine avec la ligne précédement sauvegardé
* effectuer une restitution des résultats (afficher surtout les mutations qui n'ont pas fais péter les tests)
* préparer des tests unitaires (mutation + rollback) (chargement de classes)
* rédiger le rapport de restitution du projet (ça peut être à plusieurs)
* (optionel) compiler le projet cible avec maven si il n'est pas compilé
* (optionel) ajouter une IHM WEB
* (optionel) executer les mutations et tests en parallèle


## Architectures

projet\
_app.kt -> point d'entrée de l'application \
_core.kt -> orchestre l'application  
_loader.kt -> chargement des classes\
_mutation.kt -> effectue les != mutations \
_repository -> sauvegarde les résultats et mutations\
_restitution.kt -> affiche les résultats \
